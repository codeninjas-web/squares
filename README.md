# Squares project #

This i a school project for the company Kong Orange, who has a sideproject called Squares.

## Disclaimer ##
This is still under development

## Short manual - NOT WORKING AT THE MOMENT - SO DON'T DO IT!!##
1. __Open terminal__
2. __$ cd 'your-project-destination'__
	- _Changes directory to project folder._
3. __$ sudo npm install__
	- _Installs node modules_
4. __ $ gulp__
	- _run default gulp task which runs watch task_
5. __Start coding__

#### Features – Gulp ####
- __Autoprefixer__
	- Autoprefix css/sass, gets info from [caniuse.com](caniuse.com)
- __BrowserSync__
	- Used for browser reload and creating server [browsersync.io](https://www.browsersync.io/)
- __Gulp-Concat__
	- Used to _combine/concat_ js files
- __Uglify__
	- Used for _minify_ js files
- __Watch__
	- Used for detect changes in files, and start tasks