﻿using Squares.Helpers;
using Squares.Models.EntityManager;
using Squares.Models.ViewModel;
using Squares.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Squares.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        // GET: Admin/Account
        [AuthorizeRoles("Administrator")]
        public ActionResult Index()
        {
            UserDataView userDataView = UserManager.GetUserDataView();

            return View(userDataView);
        }

        // POST: Delete Account
        [HttpPost]
        [AuthorizeRoles("Administrator")]
        [NoDirectAccess]
        public ActionResult Delete(int id)
        {
            UserManager.DeleteUser(id);

            return Json(new { success = true });
        }
    }
}