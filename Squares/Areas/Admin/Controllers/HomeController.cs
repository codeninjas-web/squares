﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Squares.Security;
using Squares.Models.ViewModel;
using Squares.Models.EntityManager;

namespace Squares.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admin/Home
        [AuthorizeRoles("Administrator")]
        public ActionResult Index()
        {
            SetDataView setDataView = SetManager.GetSetDataView();

            return View(setDataView);
        }
    }
}