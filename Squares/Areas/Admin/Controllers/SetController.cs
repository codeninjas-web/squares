﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Squares.Models.EntityManager;
using Squares.Helpers;
using Squares.Security;
using System.IO;

namespace Squares.Areas.Admin.Controllers
{
    public class SetController : Controller
    {
        // POST: Delete Set
        [HttpPost]
        [AuthorizeRoles("Administrator")]
        [NoDirectAccess]
        public ActionResult Delete(int id)
        {
            SetManager.DeleteSet(id);

            return Json(new { success = true });
        }
    }
}