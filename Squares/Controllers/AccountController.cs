﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Squares.Models.DB;
using Squares.Models.ViewModel;
using Squares.Models.EntityManager;
using Squares.Helpers;
using System.Web.Helpers;

namespace Squares.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        // GET: Account Details Menu
        [ChildActionOnly]
        public ActionResult UserDetailsMenu()
        {
            CurrentUser currentUser = UserManager.GetCurrentUser();

            return PartialView("_UserDetailsMenu", currentUser);
        }

        // GET: Account/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            UserDetailsView userDetailsView = UserManager.GetUserDetailsView((int) id);
            if (userDetailsView.User == null)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(userDetailsView);
        }

        [HttpPost]
        public ActionResult Signup(UserSignupView userSignupView)
        {
            if (UserManager.GetCurrentUser() != null)
                return RedirectToAction("Index", "Home");

            if (UserManager.GetUserByEmail(userSignupView.UserEmail) != null)
                return Json(new { success = false, message = "Email is already taken" });

            User user = new User();
            user.UserRoleID = 2;
            user.UserEmail = userSignupView.UserEmail;
            user.UserPassword = UserManager.PasswordManager.GeneratePassword(userSignupView.UserPassword);
            user.UserFirstName = userSignupView.UserFirstName;
            user.UserLastName = userSignupView.UserLastName;

            if (userSignupView.UserCountry == null)
                user.UserCountry = "";
            else
                user.UserCountry = userSignupView.UserCountry;

            if (userSignupView.UserCity == null)
                user.UserCity = "";
            else
                user.UserCity = userSignupView.UserCity;

            if (userSignupView.UserWebsite == null)
                user.UserWebsite = "";
            else
                user.UserWebsite = userSignupView.UserWebsite;

            if (userSignupView.UserProfession == null)
                user.UserProfession = "";
            else
                user.UserProfession = userSignupView.UserProfession;

            user.UserDescription = "";

            if (userSignupView.UserProfileImage == null)
            {
                user.UserProfileImage = "";
                UserManager.CreateUser(user);
            }
            else
            {
                UserManager.CreateUser(user);
                SaveImage(userSignupView.UserProfileImage);
            }

            return Json(new { success = true });
        }

        private void SaveImage(HttpPostedFileBase file)
        {
            User insertedUser = UserManager.GetLatestInsertedUser();

            //Save file content goes here
            if (file != null && file.ContentLength > 0)
            {
                string path = Request.MapPath("~/Content/img/users/user" + insertedUser.UserID);

                bool isExists = System.IO.Directory.Exists(path);
                if (!isExists)
                    System.IO.Directory.CreateDirectory(path);

                WebImage image = new WebImage(file.InputStream);
                image.FileName = "user" + insertedUser.UserID + "-profileimage";

                UserManager.UpdateUserProfileImage(insertedUser.UserID, image.FileName + "." + image.ImageFormat);

                // Saving thumbnail image name for use later on
                string imageThumbnailName = image.FileName + "-thumbnail";

                // Save original image
                image.Save(path + "/" + image.FileName);

                // Resize and save thumbnail image
                image = image.Resize(101, 101); // Will end at 100x100px check comment below for more
                image = image.Crop(1, 1); // Cropping it to remove 1px border at top and left sides (bug in WebImage)

                image.Save(path + "/" + imageThumbnailName);
            }
        }

        // GET: Account Login
        [HttpGet]
        public ActionResult Login()
        {
            if (UserManager.GetCurrentUser() != null)
                return RedirectToAction("Index", "Home");

            return View();
        }

        // POST: Account Login
        [HttpPost]
        public ActionResult Login(UserLoginView userLoginView)
        {
            User user = UserManager.GetUserByEmail(userLoginView.UserEmail);
            object response;

            if (user != null)
            {
                string userPassword = user.UserPassword;

                string salt = userPassword.Split(':')[0];
                string hash = userPassword.Split(':')[1];

                bool isPasswordMatch = UserManager.PasswordManager.IsPasswordMatch(userLoginView.UserPassword, salt, hash);

                if (isPasswordMatch)
                {
                    FormsAuthentication.SetAuthCookie(userLoginView.UserEmail, false);
                    response = new
                    {
                        success = true
                    };
                    //return RedirectToAction("Index", "Home");
                }
                else
                {
                    response = new
                    {
                        success = false,
                        message = "Wrong email or password"
                    };
                }
            }
            else
            {
                response = new
                {
                    success = false,
                    message = "Wrong email or password"
                };
            }

            return Json(response);
        }
        

            // POST: Account Login
        [HttpGet]
        [NoDirectAccess]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}