﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Squares.Models.ViewModel;
using Squares.Models.DB;
using Squares.Models.EntityManager;
using System.Data.SqlTypes;
using System.Web.Helpers;
using Squares.Helpers;

namespace Squares.Controllers
{
    public class SetController : Controller
    {
        // GET: Set
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        // GET: Set Combine
        public ActionResult Combine(int? id)
        {
            if (id == null)
                return RedirectToAction("Index", "Home");

            SetCombineView setCombineView = SetManager.GetSetCombineView((int) id);

            if (setCombineView.Set == null)
                return RedirectToAction("Index", "Home");

            return View(setCombineView);
        }

        public IEnumerable<SelectListItem> GetCategories()
        {
            List<SelectListItem> categories = CategoryManager.GetCategoriesForDropDown();
            return new SelectList(categories, "Value", "Text");
        }

        // GET: Create Set
        [HttpGet]
        public ActionResult Create()
        {
            SetCreateView setCreateView = new SetCreateView();
            setCreateView.Categories = GetCategories();

            return View(setCreateView);
        }

        private void CreatePieces()
        {
            Set insertedSet = SetManager.GetLatestInsertedSet();

            int pieceNumber = 1;
            string imageName = "";
            string imageFormat = "";

            foreach (string requestFileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[requestFileName];
                //Save file content goes here
                if (file != null && file.ContentLength > 0)
                {
                    string path = Request.MapPath("~/Content/img/sets/set" + insertedSet.SetID);

                    bool isExists = System.IO.Directory.Exists(path);
                    if (!isExists)
                        System.IO.Directory.CreateDirectory(path);

                    WebImage image = new WebImage(file.InputStream);
                    image.FileName = "set" + insertedSet.SetID + "-pic" + pieceNumber;

                    if (pieceNumber == 1)
                        SetManager.UpdateSetCoverImage(insertedSet.SetID, image.FileName + "." + image.ImageFormat);
                    

                    // For some reason the WebImage changes its FileName on save, so I have to
                    // save theese in a variable to use them to create a piece in the db later on.
                    imageName = image.FileName;
                    imageFormat = image.ImageFormat;

                    image.Save(path + "/" + imageName);
                }

                Piece piece = new Piece();
                piece.PieceTitle = "PieceTitle Test!";
                piece.PieceImage = imageName + "." + imageFormat;
                piece.PieceSetID = insertedSet.SetID;
                PieceManager.CreatePiece(piece);

                pieceNumber++;
            }
        }

        // POST: Create Set
        [HttpPost]
        public ActionResult Create(SetCreateView setCreateView)
        {
            CurrentUser currentUser = UserManager.GetCurrentUser();

            Set set = new Set();
            set.SetTitle = setCreateView.SetTitle;
            set.SetDescription = setCreateView.SetDescription;
            set.SetCreatedDate = DateTime.Now;
            set.SetCategoryID = setCreateView.SelectedCategory;
            set.SetUserID = currentUser.UserID;
            set.SetCoverImage = "";
            SetManager.CreateSet(set);

            CreatePieces();

            return Json(Url.Action("Index", "Home")); // Christian skal sørge for at redirecte ved hjælp af : window.location.href i den metode som modtager dette response.
        }

        // POST: Delete Set
        [HttpPost]
        [NoDirectAccess]
        public ActionResult Delete(int id)
        {
            CurrentUser currentUser = UserManager.GetCurrentUser();
            Set set = SetManager.GetSetByID(id);

            if (set.SetUserID != currentUser.UserID)
                return RedirectToAction("Index", "Home");

            SetManager.DeleteSet(id);

            return Json(new { success = true });
        }
    }
}