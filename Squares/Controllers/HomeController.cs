﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Squares.Models.ViewModel;
using Squares.Models.EntityManager;

namespace Squares.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            SetDataView setDataView = SetManager.GetSetDataView();

            return View(setDataView);
        }

        public ActionResult LikeDislikeSet(int setID)
        {
            CurrentUser currentUser = UserManager.GetCurrentUser();

            if (currentUser == null)
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);

            bool setAlreadyLiked = SetManager.IsSetLiked(setID, currentUser.UserID);

            if (setAlreadyLiked)
                SetManager.DislikeSet(setID, currentUser.UserID);
            else
                SetManager.LikeSet(setID, currentUser.UserID);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}