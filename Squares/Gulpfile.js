/**
 * Node modules
 */
 var  gulp          = require('gulp'),                      // use gulp
      plumber       = require('gulp-plumber'),              // Keep task running on error
      browserSync   = require('browser-sync').create(),     // local server and browser reload
      reload        = browserSync.reload,                   // local server and browser reload
      autoprefixer  = require('gulp-autoprefixer'),         // prefix css, uses caniuse.com to check for prefixes
      concat        = require('gulp-concat'),               // For combining files together
      jshint        = require('gulp-jshint'),               // Debug js code in console, show errors
      uglify        = require('gulp-uglify'),               // minify files
      sass          = require('gulp-sass'),                 // process sass files to css
      rename        = require('gulp-rename'),               // rename/suffex filenames
      sourcemaps    = require('gulp-sourcemaps');           // Create map files toshow linenumbers to concatenated files


/**
 * Path to useful folders
 */
var content 		= 'Content/',
    cssMvc          = 'Content/css/',
    jsMvc           = 'Content/js/',
    imgMvc          = 'Content/img/',
    frontend 		= 'Views/Frontend/',
    css 			= 'Views/Frontend/Content/css/',
    js 				= 'Views/Frontend/Content/js/',
    vendor 			= 'Views/Frontend/Content/vendor/',
    scss 			= 'Views/Frontend/Content/scss/',
    partials 		= 'Views/Frontend/Content/partials/';


/**
 * Error message to show in console
 */
var cssError = function(err) {
    console.log('Error: ' + err.message);
    this.emit('end');
};


var errorSass = function(err) {
    console.log('Error: ' + err.message);
    this.emit('end'); //End function
};

/**
 * Sass to css and autoprefix
 */
gulp.task('sass', function() {
    return gulp.src(scss + '**/*.scss')
        .pipe(plumber({ errorHandler: errorSass}))
        .pipe(sourcemaps.init())
        .pipe(sass({
            sourceMap: true,
            outputStyle: 'compressed' // options: nested, expanded, compact, compressed
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename({
            suffix: ".min",
            extname: ".css"
        }))
        .pipe(gulp.dest(cssMvc)) // destination to MVC css
        .pipe(sourcemaps.write('./maps')) // source to css files
        .pipe(gulp.dest(css)) // destination to Frontend
        .pipe(browserSync.stream()); //inject new CSS styles into the browser whenever the sass task is ran.
});


/**
 * JS Error message to show in console
 */
var jsError = function(err) {
    console.log('Error: ' + err.message);
    this.emit('end');
};


/**
 * Scripts task
 *
 */
gulp.task('scripts', function() {
    return gulp.src(js + '**/*.js')
        .pipe(plumber({
          errorHandler: jsError
        }))
        .pipe(sourcemaps.init()) // initialize sourcemaps
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(concat('main.js'))
        .pipe(uglify()) // minify files
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(jsMvc))
        .pipe(browserSync.stream());
});


/**
 * Compress js
 *
 */
gulp.task('compress', function() {
    return gulp.src([
        vendor + 'jquery-2.2.3.min.js',
        vendor + 'modernizr-2.8.3.min.js',
        vendor + 'jquery-ui.min.js ',
        vendor + 'bootstrap.min.js',
        vendor + 'dropzone.js',
        vendor + 'imagesloaded.pkgd.min.js',
        vendor + 'isotope.min.js',
        vendor + 'jquery.ui.touch-punch.min.js',
        vendor + 'jquery.validate.min.js',
        jsMvc +  'main.js'
    ])
    .pipe(plumber({
        errorHandler: jsError
    }))
    .pipe(concat('main.min.js'))
    .pipe(uglify()) // minify files
    .pipe(gulp.dest(jsMvc))
    .pipe(browserSync.stream());
});


/**
* BrowserSync
* For browserreload and creating server
* Documentation: https://browsersync.io/docs/gulp/
*/
gulp.task('server', function() {
	browserSync.init({
		server: {
			baseDir: './' + frontend // root folder for localhost
		},
        notify: false,
		open: false // open tab in browser
	});

    // gulp.watch(devcss + '**/*.css', ['cssTask']); // watch for css files and run task
    gulp.watch(scss + '**/*.scss', ['sass']); // watch for css files and run task
    gulp.watch(js + '**/*.js', ['scripts']);
    gulp.watch(frontend + '**/*.html').on('change', browserSync.reload);
});


/**
 * Register gulp tasks
 */

// gulp dev : for development
gulp.task('dev', ['server', 'compress']);

// gulp live : for production
gulp.task('live', ['compress']);
