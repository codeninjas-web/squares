﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Squares.Models.DB;
using Squares.Models.ViewModel;
using Squares.Models.EntityManager;

namespace Squares.Security
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        private readonly string[] userAssignedRoles;

        public AuthorizeRolesAttribute(params string[] roles)
        {
            this.userAssignedRoles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            CurrentUser currentUser = UserManager.GetCurrentUser();

            if (currentUser == null)
                return false;

            foreach (var role in userAssignedRoles)
            {
                if (currentUser.UserRoleTitle == role)
                    return true;
            }

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("~/");
        }

    }
}