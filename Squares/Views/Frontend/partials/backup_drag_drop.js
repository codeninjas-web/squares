$(document).ready(function() {
	closeInstuction();


	if ( $('.combine--container').length >= 1 ) {
		dragAndDrop();
	}

	if ( $('#sets--container').length >= 1 ) {
		initIsotope();
	}
});


/**
 * CLose instructions on frontpage
 */
function closeInstuction() {
	var $closeBtn 		= $('#closeInstructions'),
		$instructions 	= $('.instructions');


	$closeBtn.click(function() {
		$instructions.slideToggle();
		$(this).toggleClass('closed');
	});
}


/**
 * Add isotope filtering and sorting to sets
 */
function initIsotope() {
	// init Isotope
	var $grid = $('.sets').isotope({
		itemSelector: '.set',
		layoutMode: 'fitRows',
		getSortData: {
			likes: '.likes parseInt',
			pieces: '.set--count parseInt',
			date: function( itemElem ) {
				var numberPattern = /\d+/g;
				var date = $( itemElem ).find('.date').text();
				return parseFloat( date.replace( /[\(\)]/g, '') );
			}
		},
		sortBy : 'likes',
		sortAscending: false
	});


	// filter functions
	var filterFns = {
		// show if number is greater than 50
		numberGreaterThan50: function() {
		var number = $(this).find('.number').text();
		return parseInt( number, 10 ) > 50;
	},

	// show if name ends with -ium
	ium: function() {
		var name = $(this).find('.name').text();
		return name.match( /ium$/ );
		}
	};

	// bind filter button click
	$('#filters').on( 'click', 'button', function() {
		var filterValue = $( this ).attr('data-filter');

		// use filterFn if matches value
		filterValue = filterFns[ filterValue ] || filterValue;
		$grid.isotope({ filter: filterValue });
	});


	// bind sort button click
	$('.options__sort').on( 'click', 'button', function() {
		var sortValue = $(this).attr('data-sort-value');
		$grid.isotope({ sortBy: sortValue });
	});

	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
		var $buttonGroup = $( buttonGroup );

		$buttonGroup.on( 'click', 'button', function() {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	});

}


/**
 * Not in use at the moment
 * Can be used for shw and hide login
 */
function showLogin() {
	var $form = $('.login__form'),
			$btn = $('#login--btn'),
			$inputs = $form.find('input');
			$submit = $form.find('.btn-login');

	$btn.click(function() {
		$form.toggleClass('show');

		if ($form.hasClass('show')) {
			$inputs.removeProp('tabindex');
			$submit.removeProp('disabled');
		} else {
			$inputs.prop('tabindex', '-1');
			$submit.prop('disabled', 'true');
		}
	});
}
