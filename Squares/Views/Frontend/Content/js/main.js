$(document).ready(function() {

	if ( $('#signupModal').length >= 1 ) {
		signupForm();
	}

	if ( $('#loginModal').length >= 1 ) {
		loginFormValidation();
	}


	if ( $('.lnkDelete').length >= 1 ) {
		$('.lnkDelete').click(function(event) {
			event.preventDefault();
		});
	}

	showHideMenu();
	closeInstuction();
	customModals();

	if ( $('.combine--container').length >= 1 ) {
		dragAndDrop();
	}

	if ( $('#sets--container').length >= 1 ) {
		initIsotope();
		like();
	}

	if ( $('#dropzone-upload').length >= 1 ) {
		stateOfForm();
		uploadImages();
	}
});

function showHideMenu() {
    var menuBtn 	= $('#nav--small'),
		menu		= $('.sidebar--left');

	menuBtn.click(function(event) {
		closeLoginModal();
		closeSignupModal();

		menu.toggleClass('hide--nav');
	});
}

/**
 * CLose instructions on frontpage
 */
function closeInstuction() {
	var $closeBtn 		= $('#closeInstructions'),
		$instructions 	= $('.instructions'),
		$body 			= $('html, body');


	$closeBtn.click(function() {
		$instructions.slideToggle();
		$(this).toggleClass('closed');

		$body.animate({
			scrollTop: 0
		}, 'fast');
	});
}


/**
 * Add isotope filtering and sorting to sets
 */
function initIsotope() {

	// init Isotope
	var $grid = $('.sets').isotope({
		itemSelector: '.set',
		layoutMode: 'fitRows',
		getSortData: {
			likes: '.likes parseInt',
			pieces: '.set--count parseInt',
			date: function ($elem) {
				var item = $($elem).find(".set__info--date").data('time');
				return item;
			}
		},
		sortBy : 'likes',
		sortAscending: false
	});


	// filter functions
	var filterFns = {
		// show if number is greater than 50
		numberGreaterThan50: function() {
		var number = $(this).find('.number').text();
		return parseInt( number, 10 ) > 50;
	},

	// show if name ends with -ium
	ium: function() {
		var name = $(this).find('.name').text();
		return name.match( /ium$/ );
		}
	};

	// bind filter button click
	$('#filters').on( 'click', 'button', function() {
		var filterValue = $( this ).attr('data-filter');

		// use filterFn if matches value
		filterValue = filterFns[ filterValue ] || filterValue;
		$grid.isotope({ filter: filterValue });
	});


	// bind sort button click
	$('.options__sort').on( 'click', 'button', function() {
		var sortValue = $(this).attr('data-sort-value');
		$grid.isotope({ sortBy: sortValue });
	});

	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
		var $buttonGroup = $( buttonGroup );

		$buttonGroup.on( 'click', 'button', function() {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	});


	// layout Isotope after each image loads
	$grid.imagesLoaded().progress( function() {
	  $grid.isotope('layout');
	});
}
