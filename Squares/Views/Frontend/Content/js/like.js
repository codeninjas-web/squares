function like() {
    var $likeButton = $('.like--btn');

    $likeButton.click(function () {
        var setid = $(this).data('setid'),
            buttonClicked = $(this),
            likesCount = buttonClicked.find('.likes'),
            number = parseInt(likesCount.text());

        buttonClicked.prop("disabled", true);

        buttonClicked.toggleClass('liked');
        if (buttonClicked.hasClass('liked')) {
            likesCount.text(number + 1);
        } else {
            likesCount.text(number - 1);
        }

        $.ajax({
            url: '/Home/LikeDislikeSet',
            dataType: 'json',
            success: function (data) {
                buttonClicked.prop("disabled", false);
            },
            data: { setID: setid }
        });
    });
}
