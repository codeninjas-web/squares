/**
 * Signup form validation
 */
function signupForm() {
	var $signup 		= $('#form__signup'),
		$nextBtn 		= $signup.find('.next'),
		$prevBtn 		= $signup.find('.previous'),
		$submit 		= $signup.find('input[type="submit"]'),
		$errorDiv 		= $signup.find('.error--container'),
		$progressbar	= $signup.find('#progressbar');


	$signup.submit(function(event) {
	    event.preventDefault();
	}).validate({
		rules: {
			password: "required",
			password_again: {
				equalTo: "#password"
			}
		},
		invalidHandler: function(event, validator) {
			var errors = validator.numberOfInvalids();

			if (errors) {
				if (errors > 1) {
					$errorDiv.find('span').html('There is <strong>' + errors + '</strong> missing fields');
				} else {
					$errorDiv.find('span').html('There is <strong>' + errors + '</strong> missing field');
				}

				$prevBtn.click();
			} else {
				$errorDiv.hide();
			}
		},
		submitHandler: function(form) {
		    //form.submit();
		    createAccount();
		}
	});

	// click next button
	$nextBtn.click(function(event) {
		$progressbar
			.find('.active')
			.toggleClass('active')
			.next()
			.toggleClass('active');

		$(this)
			.closest('.form__signup--fieldset')
			.removeClass('onscreen')
			.addClass('animate-left')
			.next()
			.removeClass('animate-right')
			.addClass('onscreen')
			.find('input').attr('tabindex', '1');

		$(this).closest('.form__signup--fieldset').find('input').attr('tabindex', '-1');

	});


	// prev button
	$prevBtn.click(function(event) {
		$progressbar
			.find('.active')
			.toggleClass('active')
			.prev()
			.toggleClass('active');

		$(this)
			.closest('.form__signup--fieldset')
			.removeClass('onscreen')
			.addClass('animate-right')
			.prev()
			.removeClass('animate-left')
			.addClass('onscreen')
			.find('input').attr('tabindex', '1');

		$(this).closest('.form__signup--fieldset').find('input').attr('tabindex', '-1');
	});
}

function createAccount() {
    var $signup             = $('#form__signup'),
		$nextBtn            = $signup.find('.next'),
		$prevBtn            = $signup.find('.previous'),
		$submit             = $signup.find('input[type="submit"]'),
		$errorDiv           = $signup.find('.error--container'),
		$progressbar        = $signup.find('#progressbar');

    var userFirstName       = $signup.find('input[name="fname"]').val(),
        userLastName        = $signup.find('input[name="lname"]').val(),
        userEmail           = $signup.find('input[name="email"]').val(),
        userPassword        = $signup.find('input[name="password"]').val(),
        userCountry         = $signup.find('input[name="country"]').val(),
        userCity            = $signup.find('input[name="city"]').val(),
        userWebsite         = $signup.find('input[name="website"]').val(),
        userProfession      = $signup.find('input[name="profession"]').val(),
        userProfileImage    = $signup.find('input[name="imageUpload"]')[0].files[0];

    var formData = new FormData();
    formData.append("UserFirstName", userFirstName);
    formData.append("UserLastName", userLastName);
    formData.append("UserEmail", userEmail);
    formData.append("UserPassword", userPassword);
    formData.append("UserCountry", userCountry);
    formData.append("UserCity", userCity);
    formData.append("UserWebsite", userWebsite);
    formData.append("UserProfession", userProfession);
    formData.append("UserProfileImage", userProfileImage);

    $.ajax({
        type: "POST",
        url: "/Account/Signup",
        data: formData,
        contentType: false, // Not to set any content header
        processData: false, // Not to process data
        success: function (data) {

            if (data.success) {
                $('#signupModal').modal('hide');
                $('#loginModal').modal('show');

                //$('#succesModal').modal('show');
            } else {
                $errorDiv.find('span').html(data.message);
                $prevBtn.click();
            }
        }
    });

}



/**
 * Login form validation
 */
function loginFormValidation() {
	var $form 			= $('#loginForm'),
		$submit 		= $form.find('input[type="submit"]'),
		$errorDiv 		= $form.find('.error--container');

	$form.validate({
		rules: {
			passwordLogin: "required"
		}
	});

	$form.submit(function (e) {
		e.preventDefault();

		var form = $(this),
		UserEmail = form.find('input[name="UserEmail"]').val(),
		UserPassword = form.find('input[name="UserPassword"]').val(),
		jsonObject = {
			"UserEmail": UserEmail,
			"UserPassword": UserPassword
		};

		$.ajax({
			url: "/Account/Login",
			type: 'POST',
			data: jsonObject,
			success: function (response) {
				$("#loginSubmit").attr("disabled", "true");

				if (response.success) {
					window.location.href = "/";
				} else {
					var wrongInfo = $('.wrong-info');

					wrongInfo.text(response.message);
					wrongInfo.fadeIn();

					setTimeout(function () {
						wrongInfo.fadeOut();
						$("#loginSubmit").removeAttr("disabled");
					}, 2000);
				}
			}
		});
	});
}
