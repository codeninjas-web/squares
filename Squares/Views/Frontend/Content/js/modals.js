function customModals() {
	closeLoginModal();

	if ($('nav').find("[data-target='#loginModal']").length >= 1) {
		$('nav').find("[data-target='#loginModal']").click(function(event) {
			closeSignupModal();
		});
	}


	var $loader = $('#loadingModal'),
		$succes = $('#succesModal');


	$loader.modal({
		keyboard: false,
		show: false,
		backdrop: 'static'
	});

	$loader.on('shown.bs.modal', function() {
		$('body').addClass('msg-backdrop');

		if ($('.sidebar').not('.hide--nav')) {
			$(this).addClass('hide--nav');
		}
	});

	$loader.on('hidden.bs.modal', function (e) {
		$('body').removeClass('msg-backdrop');
	});

	$succes.modal({
		keyboard: false,
		show: false,
		backdrop: 'static'
	});

	$succes.on('shown.bs.modal', function() {
		$('body').addClass('msg-backdrop');
	});

	$succes.on('hidden.bs.modal', function (e) {
		$('body').removeClass('msg-backdrop');
	});


	$('#loginModal').on('shown.bs.modal', function (e) {
		closeSideBar();
	});

	$('#signupModal').on('shown.bs.modal', function (e) {
		closeSideBar();
	});

	$('#loginModal').on('hidden.bs.modal', function (e) {
		$('#loginModal').find('form')[0].reset();
	});

	$('#signupModal').on('hidden.bs.modal', function (e) {
		$('#signupModal').find('form')[0].reset();
	});
}


function closeLoginModal() {
	$('#loginModal').modal('hide');
}

function closeSignupModal() {
	$('#signupModal').modal('hide');
}

function closeSideBar() {
	$('.sidebar').addClass('hide--nav');
}
