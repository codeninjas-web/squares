// change state of submit button on upload form
function stateOfForm() {
	var $pieces 		= $('.dz-preview'),
		numberPieces 	= $('#dropzone-upload').children('.dz-preview').length;

	// console.log($('#dropzone-upload').children('.dz-preview').length);

	if (numberPieces > 4 ) {
		checkTitle();
	} else {
		disableSubmitUpload();
	}

}

function checkTitle() {
	var $title = $('#setTitle');

	if ($title.val().length > 0) {
		enableSubmitUpload();
	} else {
		$title.focus(function(event) {
			if ($(this).val().length > 0) {
				enableSubmitUpload();
			} else {
				disableSubmitUpload();
			}
		}).blur(function(event) {
			if ($(this).val().length > 0) {
				enableSubmitUpload();
			} else {
				disableSubmitUpload();
			}
		});
	}
}

// enable upload form
function enableSubmitUpload() {
	$('#uploadFiles').removeAttr('disabled');
	$('#uploadFiles').removeClass('not-active');
	$('#uploadFiles').val('Create Set');
}

// disable upload form
function disableSubmitUpload() {
	$('#uploadFiles').attr('disabled', 'true');
	$('#uploadFiles').addClass('not-active');
	$('#uploadFiles').val('Create Set - Must have images and a title');
}


function uploadImages() {
	Dropzone.autoDiscover = false; // prevents dropzone from auto detect dropzone function

	var uploadForm 	= $('#dropzone-upload');
		$submit 	= uploadForm.find('#uploadFiles');

	uploadForm.dropzone({
		addRemoveLinks: false,
		maxFilesize: 4,				// max file size to 4mb
		autoProcessQueue: false, 	// Dropzone should wait for the user to click a button to upload
		uploadMultiple: true,  		// Dropzone should upload all files at once (including the form data) not all files individually
		parallelUploads: 100,  		// that means that they shouldn't be split up in chunks as well
		maxFiles: 25, 				// and this is to make sure that the user doesn't drop more files than allowed in one request.
		thumbnailWidth: 200,
		thumbnailHeight: 200,
		clickable: '#dz-message',	// specify
		acceptedFiles: '.jpg, .jpeg, .png', 	// html5 definition of image types https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input#attr-accept

		init: function() {
			var myDropzone 	= this,
				$title 			= $('#setTitle'),
				$titleVal 		= $('#setTitle').val();

			$submit.click(function(event) {
				event.preventDefault();
				event.stopPropagation();

				if ( $('#setTitle').val().length !== 0 ) {
				    $('.form__required').removeClass('error');
				    $('#loadingModal').modal('show');
					myDropzone.processQueue();
				} else {
					$('.form__required').addClass('error');
				}
			});

			var totalfiles = 0;

			myDropzone.on('maxfilesexceeded', function(file) {
				var _this = this;
				_this.removeFile(file);
				totalfiles++;
			});

			myDropzone.on('removedfile', function() {
				var numberPieces = myDropzone.getQueuedFiles().length;

				if (numberPieces < 5) {
					disableSubmitUpload();
				}
			});

			myDropzone.on('addedfile', function(file) {
				var removeButton = Dropzone.createElement('<button class="removePiece"><i class="fa fa-trash" aria-hidden="true"></i></button>');

				// Capture the Dropzone instance as closure.
				var _this = this;

				// Listen to the click event
				removeButton.addEventListener("click", function(e) {
					// Make sure the button click doesn't submit the form:
					e.preventDefault();
					e.stopPropagation();

					// Remove the file preview.
					_this.removeFile(file);
				});

				// // Add the button to the file preview element.
				file.previewElement.appendChild(removeButton);

				var numberPieces = myDropzone.getQueuedFiles().length;

				if (numberPieces >= 4) {
					stateOfForm();
				}
			});

			myDropzone.on("queuecomplete", function (file, responseText) {
			    setTimeout(function () {
			        $("#loadingModal").modal('hide');

			        $("#succesModal").modal('show');
			        setTimeout(function () {
			            window.location.href = '/';
			        }, 1000);
			    }, 500);
			});
		}
	});
}
