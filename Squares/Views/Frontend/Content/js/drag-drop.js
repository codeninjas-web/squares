/**
 * Drag and drop function for pieces
 */
function dragAndDrop() {
	// Variables
	var $piece			= $( "#piece" ),
		$canvases 		= $('#combine__canvas'),
		$dragPiece 		= $( 'div.piece__item' ),
		$canvasBox 		= $('.combine__box'),

		// highlight
		canvasHighlight = 'combine__box--highlight',

		// canvases to accept
		$canvasAccept	= $('#piece > div'),
		removePiece = '<button class="removePiece btn btn--green">Remove piece</button>';

	// draggable items
	$dragPiece.draggable({
		revert: 'invalid',
		containment: '.combine--container',
		addClasses: '.animated .rubberBand',
		helper: 'clone',
		cursor: 'move'
    });


	/**
	 * canvases
	 */

	$canvasBox.each(function(index, el) {
		var box_name = el.classList[1],
			$combineCanvas = $('.' + box_name);

		// droppeable canvas
		$combineCanvas.droppable({
			accept: $canvasAccept,
			tolerance: 'intersect',
			activeClass: canvasHighlight,
			drop: function( event, ui ) {
				if ( $(this).find('.canvas__bg').length > 0 ) {
					$(this).find('.canvas__bg').remove();
				}


				var bgImg = event.originalEvent.target.src,
					$removePiece = $('<button class="remove-piece"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>'),
					$newCanvas	= $('<div class="canvas__bg showBg" style="background-image: url('+ bgImg +')">');

					$removePiece.appendTo($newCanvas);

				$newCanvas.fadeIn().appendTo($(this));
				removeCurrentPiece();
			}
		});
	});


	$('.piece__item' ).click(function( event ) {
		var $item 	= $( this ),
			$target = $( event.target );

		if ( $target.is('.removePiece') ) {
			movePieceBackToList( $item );
		}

		return false;
	});


	function movePieceBackToList( $item ) {
		$item.fadeOut(function() {
			$item.find('.removePiece').remove().end().appendTo( $piece ).fadeIn();
		});
	}


	function removeCurrentPiece() {
		$('.remove-piece').click(function(event) {
			$(this).parent().remove();
		});
	}
}
