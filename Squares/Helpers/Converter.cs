﻿using Squares.Models.ViewModel;
using Squares.Models.DB;

namespace Squares.Helpers
{
    public static class Converter
    {
        /*
        public static UserModel ConvertUserToUserModel(User user)
        {
            if (user == null)
                return null;

            UserModel userModel = new UserModel();
            userModel.ID = user.UserID;
            userModel.Role = ConvertRoleToRoleModel(user.Role);
            userModel.Email = user.UserEmail;
            userModel.Password = user.UserPassword;
            userModel.FirstName = user.UserFirstName;
            userModel.LastName = user.UserLastName;
            userModel.Country = user.UserCountry;
            userModel.City = user.UserCity;
            userModel.Website = user.UserWebsite;
            userModel.Profession = user.UserProfession;
            userModel.Description = user.UserDescription;
            userModel.ProfileImage = user.UserProfileImage;

            foreach (Set set in user.Sets)
            {
                SetModel setModel = ConvertSetToSetModel(set, userModel: userModel);
                
                userModel.Sets.Add(setModel);
            }

            foreach (Like like in user.Likes)
            {
                LikeModel likeModel = ConvertLikeToLikeModel(like, userModel: userModel);

                userModel.Likes.Add(likeModel);
            }

            foreach (Like like in user.Likes)
            {
                LikeModel likeModel = null;

                if (setModel == null)
                    likeModel = ConvertLikeToLikeModel(like, userModel: userModel);
                else
                    likeModel = ConvertLikeToLikeModel(like, userModel: userModel, setModel: setModel);

                userModel.Likes.Add(likeModel);
            }

            return userModel;
        }

        public static SetModel ConvertSetToSetModel(Set set, UserModel userModel = null)
        {
            if (set == null)
                return null;

            SetModel setModel = new SetModel();
            setModel.ID = set.SetID;
            setModel.Category = ConvertCategoryToCategoryModel(set.Category);

            if (userModel != null)
            {
                setModel.User = userModel;
            }
            else
            {
                setModel.User = ConvertUserToUserModel(set.User);
            }
            
            setModel.Title = set.SetTitle;
            setModel.Description = set.SetDescription;
            setModel.CoverImage = set.SetCoverImage;
            setModel.CreatedDate = set.SetCreatedDate;

            foreach (Like like in set.Likes)
            {
                LikeModel likeModel = ConvertLikeToLikeModel(like, userModel: setModel.User);
                setModel.Likes.Add(likeModel);
            }

            foreach (Piece piece in set.Pieces)
            {
                PieceModel pieceModel = ConvertPieceToPieceModel(piece);
                setModel.Pieces.Add(pieceModel);
            }

            return setModel;
        }

        public static RoleModel ConvertRoleToRoleModel(Role role)
        {
            if (role == null)
                return null;

            RoleModel roleModel = new RoleModel();
            roleModel.ID = role.RoleID;
            roleModel.Title = role.RoleTitle;

            return roleModel;
        }

        public static CategoryModel ConvertCategoryToCategoryModel(Category category)
        {
            if (category == null)
                return null;

            CategoryModel categoryModel = new CategoryModel();
            categoryModel.ID = category.CategoryID;
            categoryModel.Title = category.CategoryTitle;

            return categoryModel;
        }

        public static PieceModel ConvertPieceToPieceModel(Piece piece)
        {
            if (piece == null)
                return null;

            PieceModel pieceModel = new PieceModel();
            pieceModel.ID = piece.PieceID;
            pieceModel.Title = piece.PieceTitle;
            pieceModel.Image = piece.PieceImage;

            return pieceModel;
        }

        public static LikeModel ConvertLikeToLikeModel(Like like, UserModel userModel = null)
        {
            if (like == null)
                return null;

            LikeModel likeModel = new LikeModel();
            likeModel.ID = like.LikeID;

            if (userModel == null)
                likeModel.User = ConvertUserToUserModel(like.User);
            else
                likeModel.User = userModel;

            return likeModel;
        }
        */
    }
}