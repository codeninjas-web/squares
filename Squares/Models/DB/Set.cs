//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Squares.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Set
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Set()
        {
            this.Likes = new HashSet<Like>();
            this.Pieces = new HashSet<Piece>();
        }
    
        public int SetID { get; set; }
        public int SetCategoryID { get; set; }
        public int SetUserID { get; set; }
        public string SetTitle { get; set; }
        public string SetDescription { get; set; }
        public string SetCoverImage { get; set; }
        public System.DateTime SetCreatedDate { get; set; }
    
        public virtual Category Category { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Like> Likes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Piece> Pieces { get; set; }
        public virtual User User { get; set; }
    }
}
