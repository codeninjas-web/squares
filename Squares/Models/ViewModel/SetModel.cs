﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Squares.Models.DB;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Squares.Models.ViewModel
{
    public class SetDataView
    {
        public List<Set> Sets { get; set; }
        public CurrentUser CurrentUser { get; set; }
    }

    public class SetCombineView
    {
        public Set Set { get; set; }
        public User SetUser { get; set; }
        public string SetUserThumbnail { get; set; }
        public List<Piece> SetPieces { get; set; }
    }

    public class SetCreateView
    {
        [Key]
        public int SetID { get; set; }

        [AllowHtml]
        [Display(Name = "Title of your set")]
        public string SetTitle { get; set; }

        [AllowHtml]
        [Display(Name = "Set description")]
        public string SetDescription { get; set; }

        [Display(Name = "Category")]
        public int SelectedCategory { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}