﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Squares.Models.DB;
using System.Web;

namespace Squares.Models.ViewModel
{
    public class UserDataView
    {
        public List<User> Users { get; set; }
        public CurrentUser CurrentUser { get; set; }
        public Dictionary<int, string> UsersThumbnail { get; set; }
    }

    public class CurrentUser
    {
        public int UserID { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public bool UserIsAdmin { get; set; }
        public string UserRoleTitle { get; set; }
        public string UserProfileImage { get; set; }
        public string UserProfileImageThumbnail { get; set; }
    }

    public class UserDetailsView
    {
        public User User { get; set; }
        public CurrentUser CurrentUser { get; set; } 
    }

    public class UserLoginView
    {
        [Key]
        public int UserID { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Email")]
        public string UserEmail { get; set; }
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string UserPassword { get; set; }
    }

    public class UserSignupView
    {
        [Key]
        public int UserID { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserCountry { get; set; }
        public string UserCity { get; set; }
        public string UserWebsite { get; set; }
        public string UserProfession { get; set; }
        public HttpPostedFileBase UserProfileImage { get; set; }
    }
}