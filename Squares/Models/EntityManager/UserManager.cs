﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Squares.Models.DB;
using Squares.Models.ViewModel;
using System.Security.Cryptography;
using System.IO;

namespace Squares.Models.EntityManager
{
    public static class UserManager
    {

        public static User GetUserByID(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                User user = new User();
                User dbUser = db.Users.Where(u => u.UserID.Equals(id)).FirstOrDefault();

                if (dbUser == null)
                    return null;

                user.UserID = dbUser.UserID;
                user.UserEmail = dbUser.UserEmail;
                user.UserPassword = dbUser.UserPassword;
                user.UserFirstName = dbUser.UserFirstName;
                user.UserLastName = dbUser.UserLastName;
                user.UserProfileImage = GetUserProfileImage(dbUser);
                user.UserDescription = dbUser.UserDescription;
                user.UserProfession = dbUser.UserProfession;
                user.UserWebsite = dbUser.UserWebsite;
                user.UserCountry = dbUser.UserCountry;
                user.UserCity = dbUser.UserCity;
                user.Sets = SetManager.GetAllSetsForUserID(dbUser.UserID);
                user.Role = RoleManager.GetRoleByID(dbUser.UserRoleID);
                user.Likes = GetLikesForUserID(dbUser.UserID);

                return user;
            }
        }

        public static User GetUserByEmail(string email)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                return db.Users.Where(u => u.UserEmail.Equals(email)).FirstOrDefault();
            }
        }

        public static User GetLatestInsertedUser()
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                User user = new User();
                User dbUser = db.Users.OrderByDescending(u => u.UserID).FirstOrDefault();

                user.UserID = dbUser.UserID;

                return user;
            }
        }

        public static List<User> GetAllUsers()
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                List<User> users = new List<User>();

                foreach (User dbUser in db.Users)
                {
                    User user = new User();
                    user.UserID = dbUser.UserID;
                    user.UserEmail = dbUser.UserEmail;
                    user.UserPassword = dbUser.UserPassword;
                    user.UserFirstName = dbUser.UserFirstName;
                    user.UserLastName = dbUser.UserLastName;
                    user.UserProfileImage = GetUserProfileImage(dbUser);
                    user.UserDescription = dbUser.UserDescription;
                    user.UserProfession = dbUser.UserProfession;
                    user.UserWebsite = dbUser.UserWebsite;
                    user.UserCountry = dbUser.UserCountry;
                    user.UserCity = dbUser.UserCity;
                    user.Sets = SetManager.GetAllSetsForUserID(dbUser.UserID);
                    user.Role = RoleManager.GetRoleByID(dbUser.UserRoleID);
                    user.Likes = GetLikesForUserID(dbUser.UserID);

                    users.Add(user);
                }

                return users;
            }
        }

        public static List<Like> GetLikesForUserID(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                List<Like> likes = new List<Like>();
                List<Like> dbLikes = db.Likes.Where(l => l.LikeUserID == id).ToList();

                foreach (Like dbLike in dbLikes)
                {
                    Like like = new Like();
                    like.LikeID = dbLike.LikeID;
                    like.LikeSetID = dbLike.LikeSetID;
                    like.LikeUserID = dbLike.LikeUserID;

                    likes.Add(like);
                }

                return likes;
            }
        }

        public static UserDataView GetUserDataView()
        {
            UserDataView userDataView = new UserDataView();
            List<User> users = GetAllUsers();

            userDataView.Users = users;
            userDataView.CurrentUser = GetCurrentUser();
            userDataView.UsersThumbnail = GetUsersThumbnail(users);

            return userDataView;
        }

        public static Dictionary<int, string> GetUsersThumbnail(List<User> users)
        {
            Dictionary<int, string> usersThumbnail = new Dictionary<int, string>();

            string thumbnail = "";
            foreach (User user in users)
            {
                thumbnail = GetUserProfileImageThumbnail(user);

                usersThumbnail.Add(user.UserID, thumbnail);
            }

            return usersThumbnail;
        }

        public static void CreateUser(User user)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
        }

        public static void UpdateUserProfileImage(int id, string profileImage)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                User user = db.Users.Where(u => u.UserID.Equals(id)).FirstOrDefault();

                user.UserProfileImage = profileImage;

                db.SaveChanges();
            }
        }

        public static void DeleteUser(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                User user = db.Users.Find(id);

                List<Set> userSets = SetManager.GetAllSetsForUserID(user.UserID);

                foreach(var set in userSets)
                {
                    SetManager.DeleteSet(set.SetID);
                }

                var likes = db.Likes.Where(l => l.LikeUserID == id);
                db.Likes.RemoveRange(likes);

                db.Users.Remove(user);
                db.SaveChanges();

                string path = HttpContext.Current.Request.MapPath("~/Content/img/users/user" + id);
                DirectoryInfo directoryInfo = new DirectoryInfo(path);

                if (directoryInfo.Exists)
                    directoryInfo.Delete(true);
            }
        }

        public static CurrentUser GetCurrentUser()
        {
            string email = HttpContext.Current.User.Identity.Name;
            User user = GetUserByEmail(email);

            if (user == null)
                return null;

            CurrentUser currentUser = new CurrentUser();
            currentUser.UserID = user.UserID;
            currentUser.UserFirstName = user.UserFirstName;
            currentUser.UserLastName = user.UserLastName;

            Role role = RoleManager.GetRoleByID(user.UserRoleID);
            currentUser.UserRoleTitle = role.RoleTitle;

            if (currentUser.UserRoleTitle == "Administrator")
                currentUser.UserIsAdmin = true;
            else
                currentUser.UserIsAdmin = false;

            currentUser.UserProfileImage = GetUserProfileImage(user);
            currentUser.UserProfileImageThumbnail = GetUserProfileImageThumbnail(user);

            return currentUser;
        }

        public static string GetUserProfileImage(User user)
        {
            if (user.UserProfileImage == "")
                return "";
            else
            {
                int dotIndex = user.UserProfileImage.IndexOf('.');
                string imageName = user.UserProfileImage.Substring(0, dotIndex);
                string imageFormat = user.UserProfileImage.Substring(dotIndex);
                return imageName + imageFormat;
            }
        }

        public static string GetUserProfileImageThumbnail(User user)
        {
            if (user.UserProfileImage == "")
                return "";
            else
            {
                int dotIndex = user.UserProfileImage.IndexOf('.');
                string imageName = user.UserProfileImage.Substring(0, dotIndex);
                string imageFormat = user.UserProfileImage.Substring(dotIndex);
                return imageName + "-thumbnail" + imageFormat;
            }  
        }

        public static UserDetailsView GetUserDetailsView(int id)
        {
            UserDetailsView userDetailsView = new UserDetailsView();

            userDetailsView.User = GetUserByID(id);
            userDetailsView.CurrentUser = GetCurrentUser();

            return userDetailsView;
        }

        #region Password Manager

        public static class PasswordManager
        {
            private static RNGCryptoServiceProvider cryptoServiceProvider = new RNGCryptoServiceProvider();
            private const int SALT_SIZE = 24;

            private static string GenerateSaltString()
            {
                byte[] saltBytes = new byte[SALT_SIZE];

                cryptoServiceProvider.GetNonZeroBytes(saltBytes);

                string saltString = Convert.ToBase64String(saltBytes);

                return saltString;
            }

            private static string GeneratePasswordSaltHashed(string passwordSalt)
            {
                // Let us use SHA256 algorithm to 
                // generate the hash from this salted password
                SHA256 sha = new SHA256CryptoServiceProvider();
                byte[] dataBytes = System.Text.Encoding.ASCII.GetBytes(passwordSalt);
                byte[] resultBytes = sha.ComputeHash(dataBytes);

                // return the hash string to the caller
                return Convert.ToBase64String(resultBytes);
            }

            public static string GeneratePassword(string plainTextPassword)
            {
                string salt = GenerateSaltString();

                string passwordSalt = plainTextPassword + salt;
                string hashedPassword = GeneratePasswordSaltHashed(passwordSalt);

                string generatedPassword = salt + ":" + hashedPassword;

                return generatedPassword;
            }

            public static bool IsPasswordMatch(string plainTextPassword, string salt, string hash)
            {
                string passwordSalt = plainTextPassword + salt;
                return hash == GeneratePasswordSaltHashed(passwordSalt);
            }
        }

        #endregion
    }
}