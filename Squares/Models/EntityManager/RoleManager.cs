using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Squares.Models.DB;

namespace Squares.Models.EntityManager
{
    public static class RoleManager
    {
        public static Role GetRoleByID(int ID)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                return db.Roles.Find(ID);
            }
        }
    }
}