﻿using Squares.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Squares.Models.EntityManager
{
    public static class CategoryManager
    {
        public static Category GetCategoryByID(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                return db.Categories.Where(c => c.CategoryID.Equals(id)).FirstOrDefault();
            }
        }

        public static List<SelectListItem> GetCategoriesForDropDown()
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                return db.Categories.Select(c =>
                                            new SelectListItem
                                            {
                                                Value = c.CategoryID.ToString(),
                                                Text = c.CategoryTitle
                                            }).ToList();
            }
        }
    }
}