﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Squares.Models.DB;
using Squares.Models.ViewModel;
using Squares.Helpers;
using System.IO;

namespace Squares.Models.EntityManager
{
    public static class SetManager
    {
        public static Set GetSetByID(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                return db.Sets.Where(s => s.SetID.Equals(id)).FirstOrDefault();
            }
        }

        public static List<Set> GetAllSets()
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                List<Set> sets = new List<Set>();

                foreach (Set set in db.Sets)
                {
                    Set s = new Set();
                    s.SetID = set.SetID;
                    s.Category = CategoryManager.GetCategoryByID(set.SetCategoryID);
                    s.User = UserManager.GetUserByID(set.SetUserID);
                    s.SetTitle = set.SetTitle;
                    s.SetDescription = set.SetDescription;
                    s.SetCoverImage = set.SetCoverImage;
                    s.SetCreatedDate = set.SetCreatedDate;
                    s.Likes = set.Likes;
                    s.Pieces = set.Pieces;

                    sets.Add(s);
                }

                return sets;
            }
        }

        public static List<Set> GetAllSetsForUserID(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                List<Set> sets = new List<Set>();
                List<Set> dbSets = db.Sets.Where(s => s.SetUserID == id).ToList();

                foreach (Set dbSet in dbSets)
                {
                    Set set = new Set();
                    set.SetID = dbSet.SetID;
                    set.Category = dbSet.Category;
                    set.User = dbSet.User;
                    set.SetTitle = dbSet.SetTitle;
                    set.SetDescription = dbSet.SetDescription;
                    set.SetCoverImage = dbSet.SetCoverImage;
                    set.SetCreatedDate = dbSet.SetCreatedDate;
                    set.Likes = dbSet.Likes;
                    set.Pieces = dbSet.Pieces;

                    sets.Add(set);
                }

                return sets;
            }
        }

        public static void UpdateSetCoverImage(int id, string coverImage)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                Set set = db.Sets.Where(s => s.SetID.Equals(id)).FirstOrDefault();

                set.SetCoverImage = coverImage;

                db.SaveChanges();
            }
        }

        public static Set GetLatestInsertedSet()
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                Set set = new Set();
                Set dbSet = db.Sets.OrderByDescending(s => s.SetID).FirstOrDefault();

                set.SetID = dbSet.SetID;
                set.Category = dbSet.Category;
                set.User = dbSet.User;
                set.SetTitle = dbSet.SetTitle;
                set.SetDescription = dbSet.SetDescription;
                set.SetCoverImage = dbSet.SetCoverImage;
                set.SetCreatedDate = dbSet.SetCreatedDate;
                set.Likes = dbSet.Likes;
                set.Pieces = dbSet.Pieces;

                return set;
            }
        }

        public static void DeleteSet(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                var pieces = db.Pieces.Where(p => p.PieceSetID == id);
                db.Pieces.RemoveRange(pieces);

                var likes = db.Likes.Where(l => l.LikeSetID == id);
                db.Likes.RemoveRange(likes);

                Set set = db.Sets.Find(id);

                db.Sets.Remove(set);
                db.SaveChanges();

                string path = HttpContext.Current.Request.MapPath("~/Content/img/sets/set" + id);
                DirectoryInfo directoryInfo = new DirectoryInfo(path);

                if (directoryInfo.Exists)
                    directoryInfo.Delete(true);
            }
        }

        public static void CreateSet(Set set)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                db.Sets.Add(set);
                db.SaveChanges();
            }
        }

        public static SetDataView GetSetDataView()
        {
            SetDataView setDataView = new SetDataView();
            List<Set> sets = GetAllSets();

            setDataView.Sets = sets;
            setDataView.CurrentUser = UserManager.GetCurrentUser();

            return setDataView;
        }

        public static SetCombineView GetSetCombineView(int id)
        {
            SetCombineView setCombineView = new SetCombineView();
            Set set = GetSetByID(id);

            setCombineView.Set = set;
            setCombineView.SetUser = UserManager.GetUserByID(set.SetUserID);
            setCombineView.SetPieces = PieceManager.GetPiecesBySetID(set.SetID);
            setCombineView.SetUserThumbnail = UserManager.GetUserProfileImageThumbnail(setCombineView.SetUser);

            return setCombineView;
        }

        public static bool IsSetLiked(int setID, int userID)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                Like like = db.Likes.Where(l => l.LikeSetID == setID && l.LikeUserID == userID).FirstOrDefault();

                return like != null;
            }
        }

        public static void LikeSet(int setID, int userID)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                Like like = new Like();
                like.LikeSetID = setID;
                like.LikeUserID = userID;

                db.Likes.Add(like);
                db.SaveChanges();
            }            
        }

        public static void DislikeSet(int setID, int userID)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                Like like = db.Likes.Where(l => l.LikeSetID == setID && l.LikeUserID == userID).FirstOrDefault();

                db.Likes.Remove(like);
                db.SaveChanges();
            }
        }
    }
}