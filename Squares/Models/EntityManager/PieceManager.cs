﻿using Squares.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Squares.Models.EntityManager
{
    public static class PieceManager
    {
        public static void CreatePiece(Piece piece)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                db.Pieces.Add(piece);
                db.SaveChanges();
            }
        }

        public static List<Piece> GetPiecesBySetID(int id)
        {
            using (DBSquaresEntities db = new DBSquaresEntities())
            {
                List<Piece> pieces = new List<Piece>();
                List<Piece> dbPieces = db.Pieces.Where(p => p.PieceSetID == id).ToList();

                foreach (Piece dbPiece in dbPieces)
                {
                    Piece piece = new Piece();
                    piece.PieceID = dbPiece.PieceID;
                    piece.PieceTitle = dbPiece.PieceTitle;
                    piece.PieceImage = dbPiece.PieceImage;

                    pieces.Add(piece);
                }

                return pieces;
            }
        }
    }
}